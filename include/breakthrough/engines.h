#ifndef BREAKTHROUGH_ENGINES
#define BREAKTHROUGH_ENGINES

#include <math.h>

#include "breakthrough/board.h"
#include "breakthrough/utils.h"
#include "breakthrough/zobrist.h"

class Move;
class Zobrist;

class Engine {
public:
  int color;
  int nb_games_played;
  int nb_games_won;
  int playouts;
  float c;
  Zobrist &z;
  std::string name;

  // Constructor
  Engine(int color, int playouts, Zobrist &z, float c = 0.4);

  // Deep copy constructor
  Engine(const Engine &p) = default;

  // 1 virtual method: the class is abstract
  // Child class musts implement this method
  virtual void play(Board &board, bool verbose) = 0;
};

class Random : public Engine {
public:
  Random(int color, int playouts, Zobrist &z, float c = 0.4)
      : Engine(color, playouts, z, c) {
    name = "random";
  }
  void play(Board &board, bool verbose);
};

class FlatMonteCarlo : public Engine {
public:
  FlatMonteCarlo(int color, int playouts, Zobrist &z, float c = 0.4)
      : Engine(color, playouts, z, c) {
    name = "Flat Monte-Carlo";
  }
  void play(Board &board, bool verbose);
};

class UCBMonteCarlo : public Engine {
public:
  UCBMonteCarlo(int color, int playouts, Zobrist &z, float c = 0.4)
      : Engine(color, playouts, z, c) {
    name = "UCB Monte-Carlo";
  }
  void play(Board &board, bool verbose);
};

class UCTMonteCarlo : public Engine {
private:
  int uct_algorithm(Board board);

public:
  UCTMonteCarlo(int color, int playouts, Zobrist &z, float c = 0.4)
      : Engine(color, playouts, z, c) {
    name = "UCT Monte-Carlo";
  }
  void play(Board &board, bool verbose);
};

class RAVEMonteCarlo : public Engine {
private:
  int rave_algorithm(Board board);

public:
  RAVEMonteCarlo(int color, int playouts, Zobrist &z, float c = 0.4)
      : Engine(color, playouts, z, c) {
    name = "RAVE Monte-Carlo";
  }
  void play(Board &board, bool verbose);
};

#endif // BREAKTHROUGH_ENGINES
