#ifndef BREAKTHROUGH_MOVE
#define BREAKTHROUGH_MOVE

#include <iostream>
#include <map>
#include <string>
#include <vector>

class Move {
public:
  int color;
  int adv_color;

  int r0; // row_origin
  int c0; // col_origin

  int r1; // row_target
  int c1; // col_target

  // Constructor with coordinates (0, 0)
  Move(int c, int rr0, int cc0, int rr1, int cc1);

  // Constructor with human point A1
  Move(int c, std::string oo, std::string tt);

  // Deep copy constructor
  Move(const Move &m) = default;

  // Destructor
  ~Move() = default;

  // Operator =
  Move &operator=(const Move &m) = default;

  // Operator < (for map and set)
  // a == b if !(a < b) && !(b < a)
  bool operator<(const Move &m) const;
};

// point format is (row, col)
// De-humanize point. Example: A1 --> (0, 0)
std::vector<int> de_humanize_point(std::string point);

// Return format is (row, col)
// Humanize point. Example: (0, 0) --> A1
std::string humanize_point(std::vector<int> coord);

#endif // BREAKTHROUGH_MOVE
