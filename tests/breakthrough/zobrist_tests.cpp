#include "Catch2/catch.hpp"
#include "breakthrough/board.h"
#include "breakthrough/zobrist.h"

TEST_CASE("Testing Zobrist hashing") {
  Zobrist z(5);
  Board board(5, 1);
  REQUIRE(board.hash == 0);

  Move m = Move(1, "A4", "A3");
  board.play_move(m, z);
  REQUIRE(board.hash != 0);

  m = Move(1, "A3", "A4");
  board.play_move(m, z);
  REQUIRE(board.hash == 0);

  m = Move(1, "A4", "A3");
  board.play_move(m, z);
  REQUIRE(board.hash != 0);
  uint64_t hash_1 = board.hash;

  m = Move(-1, "E2", "E3");
  board.play_move(m, z);
  m = Move(-1, "E3", "E2");
  board.play_move(m, z);

  REQUIRE(board.hash == hash_1);
}

TEST_CASE("Testing AMAF map") {
  Zobrist z(5);
  uint64_t b = 0;

  REQUIRE(z.amaf_wins[b].size() == 0);
  REQUIRE(z.amaf_playouts[b].size() == 0);

  Move m1 = Move(1, "A3", "A4");
  z.amaf_wins[b][m1] += 1;
  z.amaf_playouts[b][m1] += 1;

  REQUIRE(z.amaf_wins[b].size() == 1);
  REQUIRE(z.amaf_playouts[b].size() == 1);
  REQUIRE(z.amaf_wins[b][m1] == 1);
  REQUIRE(z.amaf_playouts[b][m1] == 1);

  Move m2 = Move(-1, "A3", "A4");
  z.amaf_wins[b][m2] = 0;
  z.amaf_playouts[b][m2] = 1;

  REQUIRE(z.amaf_wins[b].size() == 2);
  REQUIRE(z.amaf_playouts[b].size() == 2);

  Move m3 = Move(1, "A3", "A4");
  z.amaf_wins[b][m3] += 1;
  z.amaf_playouts[b][m3] += 1;

  REQUIRE(z.amaf_wins[b].size() == 2);
  REQUIRE(z.amaf_playouts[b].size() == 2);

  REQUIRE(z.amaf_wins[b][m1] == 2);
  REQUIRE(z.amaf_playouts[b][m1] == 2);
}

TEST_CASE("Testing AMAF map, bis") {
  Zobrist z(5);
  uint64_t b = 0;

  REQUIRE(z.amaf_wins[b].size() == 0);
  REQUIRE(z.amaf_playouts[b].size() == 0);

  Move m1 = Move(1, "A3", "A4");
  z.amaf_wins[b][m1] += 1;
  z.amaf_playouts[b][m1] += 1;

  Move m2 = Move(1, "A4", "A5");
  z.amaf_wins[b][m2] += 1;
  z.amaf_playouts[b][m2] += 1;

  Move m3 = Move(1, "C4", "B5");
  z.amaf_wins[b][m3] += 1;
  z.amaf_playouts[b][m3] += 1;

  Move m4 = Move(1, "B3", "A5");
  z.amaf_wins[b][m4] += 1;
  z.amaf_playouts[b][m4] += 1;

  Move m5 = Move(1, "C4", "A5");
  z.amaf_wins[b][m5] += 1;
  z.amaf_playouts[b][m5] += 1;

  REQUIRE(z.amaf_wins[b].size() == 5);
  REQUIRE(z.amaf_playouts[b].size() == 5);

  Move m6 = Move(1, "C4", "B5");
  z.amaf_wins[b][m6] += 1;
  z.amaf_playouts[b][m6] += 1;

  REQUIRE(z.amaf_wins[b].size() == 5);
  REQUIRE(z.amaf_playouts[b].size() == 5);

  REQUIRE(z.amaf_wins[b][m3] == 2);
  REQUIRE(z.amaf_playouts[b][m3] == 2);
}
