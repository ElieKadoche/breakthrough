#include "Catch2/catch.hpp"
#include "breakthrough/board.h"
#include "breakthrough/move.h"

TEST_CASE("Testing is_move_valid and play_move") {
  Board board(5, 1);
  Zobrist z(5);

  Move m(1, "B1", "B2");
  REQUIRE(board.is_move_valid(m) == false);

  m = Move(-1, "C2", "C3");
  REQUIRE(board.is_move_valid(m) == false);

  m = Move(1, "A4", "A3");
  REQUIRE(board.is_move_valid(m) == true);
  board.play_move(m, z);

  m = Move(-1, "D2", "D3");
  REQUIRE(board.is_move_valid(m) == true);
  board.play_move(m, z);

  m = Move(-1, "C4", "C3");
  REQUIRE(board.is_move_valid(m) == false);

  m = Move(1, "C4", "C3");
  REQUIRE(board.is_move_valid(m) == true);
  board.play_move(m, z);

  m = Move(-1, "B2", "A3");
  REQUIRE(board.is_move_valid(m) == true);
  board.play_move(m, z);

  m = Move(1, "C3", "C4");
  REQUIRE(board.is_move_valid(m) == false);

  m = Move(1, "E4", "D3");
  REQUIRE(board.is_move_valid(m) == true);
  board.play_move(m, z);
}

TEST_CASE("Testing deep copy") {
  Board board(5, 1);
  Zobrist z(5);
  Board copy = board;

  Move m(1, "A4", "A3");
  board.play_move(m, z);

  REQUIRE(board.turn == -1);
  REQUIRE(copy.turn == 1);

  REQUIRE(board.board[2][0] == 1);
  REQUIRE(board.board[3][0] == 0);
  REQUIRE(copy.board[2][0] == 0);
  REQUIRE(copy.board[3][0] == 1);
}
