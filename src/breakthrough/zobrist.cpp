#include "breakthrough/zobrist.h"

Zobrist::Zobrist(int n) {
  this->n = n;

  for (int row = 0; row < n; ++row) {
    for (int col = 0; col < n; ++col) {
      std::tuple<int, int, int> black{-1, row, col};
      std::tuple<int, int, int> white{1, row, col};

      uint64_t black_hash = rand() % UINT64_MAX;
      uint64_t white_hash = rand() % UINT64_MAX;

      hashes[black] = black_hash;
      hashes[white] = white_hash;
    }
  }
}

uint64_t Zobrist::get_hash(int color, int row, int col) {
  std::tuple<int, int, int> key{color, row, col};
  return hashes[key];
}

uint64_t Zobrist::get_next_hash(uint64_t hash, const Move &move) {
  uint64_t origin_hash = get_hash(move.color, move.r0, move.c0);
  uint64_t target_hash = get_hash(move.color, move.r1, move.c1);

  return hash ^ origin_hash ^ target_hash;
}

void Zobrist::clear_transposition_table() {
  table_total_playouts.clear();
  table_playouts_per_child.clear();
  table_wins_per_child.clear();
  amaf_wins.clear();
  amaf_playouts.clear();
}
