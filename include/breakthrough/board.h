#ifndef BREAKTHROUGH_BOARD
#define BREAKTHROUGH_BOARD

#include <iostream>

#include "breakthrough/move.h"
#include "breakthrough/zobrist.h"

class Move;
class Zobrist;

/** A board looks like that
 *
 *   A | B| C| D| E
 * 1|-1|-1|-1|-1|-1|1
 * 2|-1|-1|-1|-1|-1|2
 * 3| 0| 0| 0| 0| 0|3
 * 4| 1| 1| 1| 1| 1|4
 * 5| 1| 1| 1| 1| 1|5
 *   A | B| C| D| E
 *
 * A1 corresponds to the coordinates (0, 0)
 * D5 corresponds to the coordinates (4, 3)
 * B3 corresponds to the coordinates (2, 1)
 * etc.
 *
 * -1 is black (red for better display)
 * 1 is white
 *
 * We can represent a board position with the Zobrist hashing. init_hash = 0
 * The full Zobrist hash table consists of #different_pieces * n * n = 50 hash
 * values hash_after_move_A = init_hash XOR hash_move_A init_hash =
 * hash_after_move_A XOR hash_move_A
 */

class Board {
public:
  int n;
  int turn;
  int **board;
  int moves_nb;
  uint64_t hash;

  // Constructor
  Board(int n = 5, int turn = -1);

  // Deep copy constructor
  Board(const Board &b);

  // Destructor
  ~Board();

  // Operator =
  Board &operator=(const Board &b);

  // Is move valid
  bool is_move_valid(const Move &move) const;

  // Play a move
  void play_move(const Move &move, Zobrist &z);

  // Winner of the game. 0 if game not finished
  int get_winner(bool misere = false) const;

  // Display the board
  void display_board() const;
};

#endif // BREAKTHROUGH_BOARD
