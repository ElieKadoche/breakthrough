#ifndef BREAKTHROUGH_ZOBRIST
#define BREAKTHROUGH_ZOBRIST

#include "breakthrough/board.h"
#include "breakthrough/move.h"

#include <cstdint>
#include <iostream>
#include <map>
#include <tuple>

class Move;

class Zobrist {
public:
  int n;
  // uint64_t empty_board; // 0, defined in Board class
  std::map<std::tuple<int, int, int>, uint64_t> hashes;

  // Transposition table
  std::map<uint64_t, int> table_total_playouts;
  std::map<uint64_t, std::vector<int>> table_playouts_per_child;
  std::map<uint64_t, std::vector<int>> table_wins_per_child;

  // AMAF (used by RAVE and GRAVE), for each move
  // For each board, the list of legal moves with their AMAF stats
  std::map<uint64_t, std::map<Move, int>> amaf_wins;
  std::map<uint64_t, std::map<Move, int>> amaf_playouts;

  // Constructor
  Zobrist(int n);

  // Deep copy constructor
  Zobrist(const Zobrist &z) = default;

  // Destructor
  ~Zobrist() = default;

  // Operator =
  Zobrist &operator=(const Zobrist &z) = default;

  // Get hash
  uint64_t get_hash(int color, int row, int col);

  // Get next hash
  uint64_t get_next_hash(uint64_t hash, const Move &move);

  // Clear transposition table
  void clear_transposition_table();
};

#endif // BREAKTHROUGH_ZOBRIST
