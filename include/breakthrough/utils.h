#ifndef BREAKTHROUGH_UTILS
#define BREAKTHROUGH_UTILS

#include "breakthrough/board.h"
#include "breakthrough/move.h"
#include "breakthrough/zobrist.h"

// Get all legal moves
std::vector<Move> get_legal_moves(const Board &board);

// Play a random game
int random_game(Board &board, Zobrist &z, int player = 0, uint64_t hash0 = 0,
                std::vector<Move> legal_moves = std::vector<Move>());

// Print legal moves
void print_legal_moves(const std::vector<Move> &legal_moves);

// Print FlatMonteCarlo results
void print_flat_monte_carlo_results(const std::vector<Move> &legal_moves,
                                    const std::vector<float> &probas_of_winning,
                                    int playouts_per_move);

// Print UCB monte carlo results
void print_ucb_monte_carlo_results(const std::vector<Move> &legal_moves,
                                   const std::vector<float> &all_ucb,
                                   const std::vector<int> &all_ni);

// Print UCT results
void print_uct_results(const std::vector<Move> &legal_moves,
                       const std::vector<int> &p);

#endif // BREAKTHROUGH_UTILS
