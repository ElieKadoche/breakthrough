#include "breakthrough/engines.h"

Engine::Engine(int color, int playouts, Zobrist &z, float c) : z(z) {
  this->color = color;
  this->playouts = playouts;
  this->z = z;
  this->c = c;
  nb_games_played = 0;
  nb_games_won = 0;
}

void Random::play(Board &board, bool verbose) {
  std::vector<Move> legal_moves = get_legal_moves(board);
  int random_index = rand() % legal_moves.size();
  board.play_move(legal_moves[random_index], z);

  if (verbose == true) {
    board.display_board();
    getchar();
  }
}
