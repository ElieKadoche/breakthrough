#include "Catch2/catch.hpp"
#include "breakthrough/board.h"
#include "breakthrough/move.h"

TEST_CASE("Testing (de_)humanize_point") {
  std::vector<int> rows(5);
  std::vector<int> cols(5);
  std::vector<std::string> points(5);

  rows = {0, 0, 2, 3, 4};
  cols = {0, 1, 2, 3, 4};
  points = {"A1", "B1", "C3", "D4", "E5"};

  for (int i = 0; i < 5; i++) {
    int row = rows[i];
    int col = cols[i];

    std::vector<int> coord(2);
    coord = {row, col};

    std::string point = points[i];

    REQUIRE(humanize_point(coord) == point);
    REQUIRE(de_humanize_point(point) == coord);
  }
}

TEST_CASE("Testing human-friendly constructor") {
  Move move(-1, "A2", "A3");
  REQUIRE(move.color == -1);
  REQUIRE(move.r0 == 1);
  REQUIRE(move.c0 == 0);
  REQUIRE(move.r1 == 2);
  REQUIRE(move.c1 == 0);
}
