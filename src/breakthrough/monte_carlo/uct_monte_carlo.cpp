#include "breakthrough/engines.h"

int UCTMonteCarlo::uct_algorithm(Board board) {
  std::vector<Move> legal_moves = get_legal_moves(board);

  if (board.get_winner() != 0) {
    return board.get_winner();
  }

  // If we never saw this position
  if (z.table_total_playouts.find(board.hash) == z.table_total_playouts.end()) {

    // We add it to the transposition table and do 1 playout on the first legal
    // move
    Board board_bis = board;
    board_bis.play_move(legal_moves[0], z);
    int winner = random_game(board_bis, z);
    if (winner == board.turn) {
      z.table_wins_per_child[board.hash] = std::vector<int>{1};
      z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
    } else {
      z.table_wins_per_child[board.hash] = std::vector<int>{0};
      z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
    }
    z.table_total_playouts[board.hash] = 1;
    z.table_total_playouts[board_bis.hash] = 1; // New node
    z.table_playouts_per_child[board.hash] = std::vector<int>{1};
    z.table_playouts_per_child[board_bis.hash] = std::vector<int>(); // New node
    return winner;
  }

  // If the position is already knwown
  else {

    // If we didn't explore all legal moves
    if (z.table_wins_per_child[board.hash].size() < legal_moves.size()) {

      // We extend the tree by playing the next legal move we didn't see (1
      // playout)
      Board board_bis = board;
      int index_next_move = z.table_wins_per_child[board.hash].size();
      board_bis.play_move(legal_moves[index_next_move], z);
      int winner = random_game(board_bis, z);
      if (winner == board.turn) {
        z.table_wins_per_child[board.hash].push_back(1);
        z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
      } else {
        z.table_wins_per_child[board.hash].push_back(0);
        z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
      }
      z.table_total_playouts[board.hash] += 1;
      z.table_total_playouts[board_bis.hash] = 1; // New node
      z.table_playouts_per_child[board.hash].push_back(1);
      z.table_playouts_per_child[board_bis.hash] =
          std::vector<int>(); // New node
      return winner;
    }

    // If we already explore all legal moves
    else {

      // We choose the best legal move
      float best_value = -999;
      int index_best_move = 0;
      for (int i = 0; i < legal_moves.size(); i++) {

        int t = z.table_total_playouts[board.hash];
        int w = z.table_wins_per_child[board.hash][i];
        int p = z.table_playouts_per_child[board.hash][i];
        float ucb_value =
            ((float)w / (float)p) + (c * sqrt(log((float)t) / (float)p));
        if (ucb_value > best_value) {
          best_value = ucb_value;
          index_best_move = i;
        }
      }

      // Recursive call
      // End of recursivity when 1 playout done
      Board board_bis = board;
      board_bis.play_move(legal_moves[index_best_move], z);
      int winner = uct_algorithm(board_bis);

      // Backup the results
      z.table_total_playouts[board.hash] += 1;
      z.table_playouts_per_child[board.hash][index_best_move] += 1;
      if (winner == board.turn) {
        z.table_wins_per_child[board.hash][index_best_move] += 1;
      }
      return winner;
    }
  }
}

void UCTMonteCarlo::play(Board &board, bool verbose) {
  int playouts_done = 0;
  std::vector<Move> legal_moves = get_legal_moves(board);

  while (playouts_done < playouts) {
    // 1 playout per call
    int win = uct_algorithm(board);
    playouts_done += 1;
  }

  // Choose the move whith highest playouts
  int index_best = 0;
  int best_ni = z.table_playouts_per_child[board.hash][0];

  for (int i = 0; i < legal_moves.size(); i++) {
    if (z.table_playouts_per_child[board.hash][i] > best_ni) {
      index_best = i;
      best_ni = z.table_playouts_per_child[board.hash][i];
    }
  }

  uint64_t old_hash = board.hash;

  // Play the move
  board.play_move(legal_moves[index_best], z);

  if (verbose == true) {
    board.display_board();
    print_uct_results(legal_moves, z.table_playouts_per_child[old_hash]);
    getchar();
  }
}
