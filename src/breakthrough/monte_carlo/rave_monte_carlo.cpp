#include "breakthrough/engines.h"

int RAVEMonteCarlo::rave_algorithm(Board board) {
  std::vector<Move> legal_moves = get_legal_moves(board);

  if (board.get_winner() != 0) {
    return board.get_winner();
  }

  // If we never saw this position
  if (z.table_total_playouts.find(board.hash) == z.table_total_playouts.end()) {

    // We add it to the transposition table and do 1 playout on the first legal
    // move
    Board board_bis = board;
    board_bis.play_move(legal_moves[0], z);

    // Playout, and we keep stats for legal moves used in the playout
    int winner = random_game(board_bis, z, color, board.hash, legal_moves);

    if (winner == board.turn) {
      z.table_wins_per_child[board.hash] = std::vector<int>{1};
      z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
      z.amaf_wins[board.hash][legal_moves[0]] += 1;                // AMAF
    } else {
      z.table_wins_per_child[board.hash] = std::vector<int>{0};
      z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
    }
    z.amaf_playouts[board.hash][legal_moves[0]] += 1; // AMAF
    z.table_total_playouts[board.hash] = 1;
    z.table_total_playouts[board_bis.hash] = 1; // New node
    z.table_playouts_per_child[board.hash] = std::vector<int>{1};
    z.table_playouts_per_child[board_bis.hash] = std::vector<int>(); // New node
    return winner;
  }

  // If the position is already knwown
  else {

    // If we didn't explore all legal moves
    if (z.table_wins_per_child[board.hash].size() < legal_moves.size()) {

      // We extend the tree by playing the next legal move we didn't see (1
      // playout)
      Board board_bis = board;
      int index_next_move = z.table_wins_per_child[board.hash].size();
      board_bis.play_move(legal_moves[index_next_move], z);

      // We add it to the transposition table and do 1 playout on the first
      // legal move
      int winner = random_game(board_bis, z, color, board.hash, legal_moves);

      if (winner == board.turn) {
        z.table_wins_per_child[board.hash].push_back(1);
        z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
        z.amaf_wins[board.hash][legal_moves[index_next_move]] += 1;  // AMAF
      } else {
        z.table_wins_per_child[board.hash].push_back(0);
        z.table_wins_per_child[board_bis.hash] = std::vector<int>(); // New node
      }
      z.amaf_playouts[board.hash][legal_moves[index_next_move]] += 1; // AMAF
      z.table_total_playouts[board.hash] += 1;
      z.table_total_playouts[board_bis.hash] = 1; // New node
      z.table_playouts_per_child[board.hash].push_back(1);
      z.table_playouts_per_child[board_bis.hash] =
          std::vector<int>(); // New node
      return winner;
    }

    // If we already explore all legal moves
    else {

      // We choose the best legal move
      float best_value = -999;
      int index_best_move = 0;
      for (int i = 0; i < legal_moves.size(); i++) {

        // UCT value
        int t = z.table_total_playouts[board.hash];
        int w = z.table_wins_per_child[board.hash][i];
        int p = z.table_playouts_per_child[board.hash][i];
        float ucb_value =
            ((float)w / (float)p) + (c * sqrt(log((float)t) / (float)p));

        // Monte-Carlo RAVE
        float bias = 1e-4;
        int pa = z.amaf_playouts[board.hash][legal_moves[i]];
        int wa = z.amaf_wins[board.hash][legal_moves[i]];
        float beta = (float)pa /
                     ((float)p + (float)pa + 4 * (float)p * (float)pa * bias);

        float amaf;
        if (pa == 0) {
          amaf = 1;
        } else {
          amaf = (float)wa / (float)pa;
        }

        float value = (1 - beta) * ucb_value + beta * amaf;

        if (value > best_value) {
          best_value = value;
          index_best_move = i;
        }
      }

      // Recursive call
      // End of recursivity when 1 playout done
      Board board_bis = board;
      board_bis.play_move(legal_moves[index_best_move], z);
      int winner = rave_algorithm(board_bis);

      // Backup the results
      z.table_total_playouts[board.hash] += 1;
      z.amaf_playouts[board.hash][legal_moves[index_best_move]] += 1; // AMAF
      z.table_playouts_per_child[board.hash][index_best_move] += 1;
      if (winner == board.turn) {
        z.table_wins_per_child[board.hash][index_best_move] += 1;
        z.amaf_wins[board.hash][legal_moves[index_best_move]] += 1; // AMAF
      }
      return winner;
    }
  }
}

void RAVEMonteCarlo::play(Board &board, bool verbose) {
  int playouts_done = 0;
  std::vector<Move> legal_moves = get_legal_moves(board);

  while (playouts_done < playouts) {
    // 1 playout per call
    int win = rave_algorithm(board);
    playouts_done += 1;
  }

  // Choose the move whith highest playouts
  int index_best = 0;
  int best_ni = z.table_playouts_per_child[board.hash][0];

  for (int i = 0; i < legal_moves.size(); i++) {
    if (z.table_playouts_per_child[board.hash][i] > best_ni) {
      index_best = i;
      best_ni = z.table_playouts_per_child[board.hash][i];
    }
  }

  uint64_t old_hash = board.hash;

  // Play the move
  board.play_move(legal_moves[index_best], z);

  if (verbose == true) {
    board.display_board();
    print_uct_results(legal_moves, z.table_playouts_per_child[old_hash]);
    getchar();
  }
}
