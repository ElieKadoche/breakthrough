#include "breakthrough/utils.h"

std::vector<Move> get_legal_moves(const Board &board) {
  std::vector<Move> legal_moves;
  for (int row = 0; row < board.n; ++row) {
    for (int col = 0; col < board.n; ++col) {
      if (board.board[row][col] == board.turn) {
        for (int k = -1; k < 2; ++k) {
          for (int i = -1; i < 2; ++i) {
            Move m(board.turn, row, col, row + k, col + i);
            if (board.is_move_valid(m) == true) {
              legal_moves.push_back(m);
            }
          }
        }
      }
    }
  }
  return legal_moves;
}

int random_game(Board &board, Zobrist &z, int player, uint64_t hash0,
                std::vector<Move> legal_moves) {
  std::vector<Move> moves_played;

  while (board.get_winner() == 0) {
    std::vector<Move> legal_moves;
    legal_moves = get_legal_moves(board);

    // Choose a random move
    int random_index = 0;
    if (legal_moves.size() > 0) {
      random_index = rand() % legal_moves.size();
    }
    Move move_to_play = legal_moves[random_index];

    if (player != 0) {
      moves_played.push_back(move_to_play);
    }

    // Play the move
    board.play_move(move_to_play, z);
  }

  if (player != 0) {
    for (int i = 0; i < moves_played.size(); ++i) {
      // If it's a legal move of initial hash
      if (z.amaf_playouts[hash0].find(moves_played[i]) !=
          z.amaf_playouts[hash0].end()) {
        z.amaf_playouts[hash0][moves_played[i]] += 1;
        if (board.get_winner() == player) {
          z.amaf_wins[hash0][moves_played[i]] += 1;
        }
      }
    }
  }

  return board.get_winner();
}

void print_legal_moves(const std::vector<Move> &legal_moves) {
  std::cout << "Legal moves: ";
  for (int i = 0; i < legal_moves.size(); ++i) {
    std::vector<int> origin_coord(2);
    std::vector<int> target_coord(2);
    origin_coord = {legal_moves[i].r0, legal_moves[i].c0};
    target_coord = {legal_moves[i].r1, legal_moves[i].c1};

    std::string origin_point;
    std::string target_point;
    origin_point = humanize_point(origin_coord);
    target_point = humanize_point(target_coord);
    std::cout << origin_point << "-->" << target_point << " ";
  }
  std::cout << std::endl;
}

void print_flat_monte_carlo_results(const std::vector<Move> &legal_moves,
                                    const std::vector<float> &probas_of_winning,
                                    int playouts_per_move) {
  for (int i = 0; i < legal_moves.size(); ++i) {
    std::vector<int> origin_coord(2);
    std::vector<int> target_coord(2);
    origin_coord = {legal_moves[i].r0, legal_moves[i].c0};
    target_coord = {legal_moves[i].r1, legal_moves[i].c1};

    std::string origin_point;
    std::string target_point;
    origin_point = humanize_point(origin_coord);
    target_point = humanize_point(target_coord);
    std::cout << origin_point << "-->" << target_point
              << " winrate = " << probas_of_winning[i] << " ("
              << playouts_per_move << " playouts)\n";
  }
}

void print_ucb_monte_carlo_results(const std::vector<Move> &legal_moves,
                                   const std::vector<float> &all_ucb,
                                   const std::vector<int> &all_ni) {
  for (int i = 0; i < legal_moves.size(); ++i) {
    std::vector<int> origin_coord(2);
    std::vector<int> target_coord(2);
    origin_coord = {legal_moves[i].r0, legal_moves[i].c0};
    target_coord = {legal_moves[i].r1, legal_moves[i].c1};

    std::string origin_point;
    std::string target_point;
    origin_point = humanize_point(origin_coord);
    target_point = humanize_point(target_coord);
    std::cout << origin_point << "-->" << target_point
              << " ucb = " << all_ucb[i] << " (" << all_ni[i] << " playouts)\n";
  }
}

void print_uct_results(const std::vector<Move> &legal_moves,
                       const std::vector<int> &p) {
  for (int i = 0; i < legal_moves.size(); ++i) {
    std::vector<int> origin_coord(2);
    std::vector<int> target_coord(2);
    origin_coord = {legal_moves[i].r0, legal_moves[i].c0};
    target_coord = {legal_moves[i].r1, legal_moves[i].c1};

    std::string origin_point;
    std::string target_point;
    origin_point = humanize_point(origin_coord);
    target_point = humanize_point(target_coord);
    std::cout << origin_point << "-->" << target_point << ": " << p[i]
              << " playouts\n";
  }
}
