#include "breakthrough/board.h"
#include "breakthrough/engines.h"
#include "breakthrough/move.h"
#include "breakthrough/utils.h"

void engine_vs_engine(Engine &engine1, Engine &engine2, int board_size) {
  Board board(board_size);

  while (board.get_winner() == 0) {
    engine1.play(board, true);
    if (board.get_winner() == 0) {
      engine2.play(board, true);
    }
  }

  std::cout << "Winner is " << board.get_winner() << std::endl;
}

void engine_vs_engine_stats(Engine &engine1, Engine &engine2, int board_size,
                            int nb_games) {
  for (int game = 0; game < nb_games; ++game) {
    Board board(board_size);

    while (board.get_winner() == 0) {
      engine1.play(board, false);
      if (board.get_winner() == 0) {
        engine2.play(board, false);
      }
    }

    if (board.get_winner() == 1) {
      engine2.nb_games_won += 1;
    } else {
      engine1.nb_games_won += 1;
    }

    std::cout << engine1.name << ' ' << engine1.nb_games_won << " --- "
              << engine2.name << ' ' << engine2.nb_games_won << '\n';

    engine1.z.clear_transposition_table();
    engine2.z.clear_transposition_table();
  }
}

int main() {
  srand(time(NULL));
  int nb_games = 100;
  int board_size = 5;
  int playouts = 300;

  Zobrist z1(board_size);
  Zobrist z2(board_size);

  // Engine 1
  // Random engine1(-1, playouts, z1);
  // FlatMonteCarlo engine1(-1, playouts, z1, 0.4);
  UCBMonteCarlo engine1(-1, playouts, z1, 0.4);
  // UCTMonteCarlo engine1(-1, playouts, z1, 0.4);
  // RAVEMonteCarlo engine1(-1, playouts, z1, 0.4);

  // Engine 2
  // Random engine2(1, playouts, z2);
  // FlatMonteCarlo engine2(1, playouts, z2, 0.4);
  // UCBMonteCarlo engine2(1, playouts, z2, 0.4);
  UCTMonteCarlo engine2(1, playouts, z2, 0.4);
  // RAVEMonteCarlo engine2(1, playouts, z2, 0.4);

  engine_vs_engine(engine1, engine2, board_size);
  engine_vs_engine_stats(engine1, engine2, board_size, nb_games);
}
