#include "breakthrough/engines.h"

void FlatMonteCarlo::play(Board &board, bool verbose) {
  std::vector<float> probas_wins;
  std::vector<Move> legal_moves = get_legal_moves(board);
  int playouts_per_move = playouts / legal_moves.size();

  // For each legal move
  for (int i = 0; i < legal_moves.size(); ++i) {
    Board board_bis = board;                // Deep copy
    board_bis.play_move(legal_moves[i], z); // Play the move
    int total_wins = 0;                     // Total wins by playing this move

    // Doing playouts (random games)
    for (int p = 0; p < playouts_per_move; ++p) {
      Board board_ter = board_bis;
      int winner = random_game(board_ter, z);
      if (winner == board.turn) {
        total_wins = total_wins + 1;
      }
    }

    probas_wins.push_back((float)total_wins / (float)playouts_per_move);
  }

  // Pick the move with the highest probability
  int index_best = 0;
  float best_proba = probas_wins[0];

  for (int i = 0; i < legal_moves.size(); ++i) {
    if (probas_wins[i] > best_proba) {
      index_best = i;
      best_proba = probas_wins[i];
    }
  }

  board.play_move(legal_moves[index_best], z); // Play the move

  if (verbose == true) {
    board.display_board();
    print_flat_monte_carlo_results(legal_moves, probas_wins, playouts_per_move);
    getchar();
  }
}
