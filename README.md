# breakthrough

Breakthrough game CPP implementation and AI.

## Installation

```
git clone https://gitlab.com/ElieKadoche/breakthrough.git
git submodule init
git submodule update
```

## Building

```
mkdir build
cd build
cmake ..
make
```

## Engines

5 engines are coded.

- Random engine.
- Upper Confidence Bound engine (UCB).
- Flat Upper Confidence bound for Trees engine (Flat UCT).
- Upper Confidence bound for Trees engine (UCT).
- Rapid Action Value Estimation for UCT (RAVE UCT).

## Statistics

For 300 playouts, on 1000 games.

- UCT: 782 wins -- Flat: 218 wins.
- UCB: 268 wins -- UCT: 732 wins.
- UCT: 678 wins -- UCB: 322 wins.
- UCT: 442 wins -- UCT: 558 wins.
- UCT: 410 wins -- RAVE: 590 wins (bias = 0.6).
