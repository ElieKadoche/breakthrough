#include "breakthrough/engines.h"

void UCBMonteCarlo::play(Board &board, bool verbose) {
  std::vector<Move> legal_moves = get_legal_moves(board);
  int playouts_done = 0; // t parameter in UCB formula

  std::vector<float> all_ucb(legal_moves.size(), 0); // See UCB formula
  std::vector<int> all_wi(legal_moves.size(), 0);    // See UCB formula
  std::vector<int> all_ni(legal_moves.size(), 0);    // See UCB formula

  // First, do 1 playout on each child move
  for (int i = 0; i < legal_moves.size(); ++i) {
    Board board_bis = board;                // Deep copy
    board_bis.play_move(legal_moves[i], z); // Play the move
    int winner = random_game(board_bis, z); // Playout

    if (winner == board.turn) {
      all_wi[i] += 1;
    }
    all_ni[i] += 1;
    playouts_done += 1;
  }

  // Compute UCB
  for (int i = 0; i < legal_moves.size(); ++i) {
    float ucb = (float)all_wi[i] / (float)all_ni[i];
    ucb = ucb + c * sqrt(log((float)playouts_done) / (float)all_ni[i]);
    all_ucb[i] = ucb;
  }

  while (playouts_done < playouts) {
    // Choose the move to study with the highest UCB
    int index_best = 0;
    float best_ucb = all_ucb[0];

    for (int i = 0; i < legal_moves.size(); i++) {
      if (all_ucb[i] > best_ucb) {
        index_best = i;
        best_ucb = all_ucb[i];
      }
    }

    Board board_bis = board;                         // Deep copy of board
    board_bis.play_move(legal_moves[index_best], z); // Play the move

    // Doing 1 playout
    int winner = random_game(board_bis, z);
    if (winner == board.turn) {
      all_wi[index_best] += 1;
    }
    all_ni[index_best] += 1;
    playouts_done += 1;

    // Update UCB
    for (int i = 0; i < legal_moves.size(); i++) {
      float ucb = (float)all_wi[i] / (float)all_ni[i];
      ucb = ucb + c * sqrt(log((float)playouts_done) / (float)all_ni[i]);
      all_ucb[i] = ucb;
    }
  }

  // Choose the move to study with the highest UCB
  int index_best = 0;
  // float best_ucb = all_ucb[0];
  int best_ni = all_ni[0]; // More robust than to take the max UCB

  for (int i = 0; i < legal_moves.size(); i++) {
    if (all_ni[i] > best_ni) {
      index_best = i;
      best_ni = all_ni[i];
    }
  }

  board.play_move(legal_moves[index_best], z);

  if (verbose == true) {
    board.display_board();
    print_ucb_monte_carlo_results(legal_moves, all_ucb, all_ni);
    getchar();
  }
}
