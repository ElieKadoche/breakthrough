#include "breakthrough/move.h"

Move::Move(int c, int rr0, int cc0, int rr1, int cc1) {
  color = c;
  adv_color = color * -1;

  r0 = rr0;
  c0 = cc0;
  r1 = rr1;
  c1 = cc1;
}

Move::Move(int c, std::string oo, std::string tt) {
  color = c;
  adv_color = color * -1;

  std::vector<int> origin = de_humanize_point(oo);
  std::vector<int> target = de_humanize_point(tt);

  r0 = origin.at(0);
  c0 = origin.at(1);

  r1 = target.at(0);
  c1 = target.at(1);
}

bool Move::operator<(const Move &m) const {
  if ((m.color < this->color) || (m.adv_color < this->adv_color) ||
      (m.r0 < this->r0) || (m.c0 < this->c0) || (m.r1 < this->r1) ||
      (m.c1 < this->c1)) {
    return true;
  }
  return false;
}

std::vector<int> de_humanize_point(std::string point) {
  char point_letter = point.at(0);
  char point_integer = point.at(1);

  std::map<char, int> col_map_de_humanize;
  col_map_de_humanize['A'] = 0;
  col_map_de_humanize['B'] = 1;
  col_map_de_humanize['C'] = 2;
  col_map_de_humanize['D'] = 3;
  col_map_de_humanize['E'] = 4;

  int row = point_integer - '0';
  row = row - 1;
  int col = col_map_de_humanize[point_letter];

  std::vector<int> coord(2);
  coord = {row, col};
  return coord;
}

std::string humanize_point(std::vector<int> coord) {
  int row = coord[0];
  int col = coord[1];

  std::map<int, char> col_map_humanize;
  col_map_humanize[0] = 'A';
  col_map_humanize[1] = 'B';
  col_map_humanize[2] = 'C';
  col_map_humanize[3] = 'D';
  col_map_humanize[4] = 'E';

  char point_col = col_map_humanize[col];

  std::string point;
  point = point_col + std::to_string(row + 1);
  return point;
}
