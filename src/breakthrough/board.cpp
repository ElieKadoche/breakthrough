#include "breakthrough/board.h"

Board::Board(int n, int turn) {
  this->n = n;
  this->turn = turn;
  moves_nb = 0;
  hash = 0; // Empty board

  // Creating and filling the board
  board = new int *[n];
  for (int row = 0; row < n; ++row) {
    board[row] = new int[n];
    for (int col = 0; col < n; ++col) {
      if (row == 0 || row == 1) {
        board[row][col] = -1; // black pawn
      } else if (row == n - 1 || row == n - 2) {
        board[row][col] = 1; // white pawn
      } else {
        board[row][col] = 0;
      }
    }
  }
}

Board::~Board() {
  for (int i = 0; i < n; ++i) {
    delete[] board[i];
  }
  delete[] board;
}

Board::Board(const Board &b) {
  n = b.n;
  turn = b.turn;
  moves_nb = b.moves_nb;
  hash = b.hash;

  // Creating and filling the board
  board = new int *[n];
  for (int row = 0; row < n; ++row) {
    board[row] = new int[n];
    for (int col = 0; col < n; ++col) {
      board[row][col] = b.board[row][col];
    }
  }
}

Board &Board::operator=(const Board &b) {
  n = b.n;
  turn = b.turn;
  moves_nb = b.moves_nb;
  hash = b.hash;

  // Creating and filling the board
  for (int row = 0; row < n; ++row) {
    for (int col = 0; col < n; ++col) {
      board[row][col] = b.board[row][col];
    }
  }
  return *this;
}

bool Board::is_move_valid(const Move &move) const {
  // Not valid if not player's turn
  if (move.color != turn) {
    return false;
  }

  // Not valid if no pawn of the player at the origin
  if (move.color != board[move.r0][move.c0]) {
    return false;
  }

  // Not valid if target is out of the board
  if (move.r1 < 0 || move.r1 >= n) {
    return false;
  }
  if (move.c1 < 0 || move.c1 >= n) {
    return false;
  }

  // Get the direction of deplacement
  int v = move.color * -1; // 1 if color -1, -1 if color 1

  // Not valid if move not on next row
  if (move.r0 + v != move.r1) {
    return false;
  }

  // Valid if target is ahead and cell empty
  if (move.c0 == move.c1 && board[move.r1][move.c1] == 0) {
    return true;
  }

  // Valid if target is ahead left and there is an adversary pawn on cell
  if (move.c1 == move.c0 - 1 && board[move.r1][move.c1] == move.adv_color) {
    return true;
  }

  // Valid if target is ahead left and there is no adversary pawn on cell
  if (move.c1 == move.c0 - 1 && board[move.r1][move.c1] == 0) {
    return true;
  }

  // Valid if target is ahead right and there is an adversary pawn on cell
  if (move.c1 == move.c0 + 1 && board[move.r1][move.c1] == move.adv_color) {
    return true;
  }

  // Valid if target is ahead right and there is no adversary pawn on cell
  if (move.c1 == move.c0 + 1 && board[move.r1][move.c1] == 0) {
    return true;
  }

  return false;
}

void Board::play_move(const Move &move, Zobrist &z) {
  board[move.r0][move.c0] = 0;
  board[move.r1][move.c1] = move.color;
  turn = move.adv_color;
  moves_nb = moves_nb + 1;
  hash = z.get_next_hash(hash, move);
}

int Board::get_winner(bool misere) const {
  // If 1 pawn on the extreme line
  for (int col = 0; col < n; ++col) {
    if (board[0][col] == 1) {
      if (!misere) {
        return 1;
      } else {
        return -1;
      }
    }
    if (board[n - 1][col] == -1) {
      if (!misere) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  // If no more pawns
  bool no_more_b = true;
  bool no_more_w = true;

  for (int row = 0; row < n; ++row) {
    for (int col = 0; col < n; ++col) {
      if (board[row][col] == -1) {
        no_more_b = false;
      }
      if (board[row][col] == 1) {
        no_more_w = false;
      }
    }
  }

  if (no_more_b == true) {
    if (!misere) {
      return 1;
    } else {
      return -1;
    }
  }
  if (no_more_w == true) {
    if (!misere) {
      return -1;
    } else {
      return 1;
    }
  }

  return 0; // If there is no winner
}

void Board::display_board() const {
  // std::cout << "  A|B|C|D|E\n";
  std::cout << "  A |B |C |D |E\n";
  for (int row = 0; row < n; ++row) {
    std::cout << row + 1 << "|";
    for (int col = 0; col < n; ++col) {
      // std::cout << board[row][col] << "|";
      if (board[row][col] == -1) {
        std::cout << "\u2B55|";
      } else if (board[row][col] == 1) {
        std::cout << "\u26AA|";
      } else {
        std::cout << "..|";
      }
    }
    std::cout << row + 1 << "\n";
  }
  std::cout << "  A |B |C |D |E\n";
  std::cout << "Move " << moves_nb << " --- Turn: " << turn << std::endl;
}
